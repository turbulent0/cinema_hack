from typing import List, Optional
from collections import OrderedDict

import torch

def avarage_weights(
    path_to_chkps : List[str],
    save_path: str,
    delete_module : bool = False,
    take_best: Optional[bool] = None,
    sort_ascending: bool = True,
    verbose: bool = True
):
    nn_weights = [torch.load(el, map_location='cpu') for el in path_to_chkps]
    nn_weights = [(el['valid_metrics']['loss'],el['model_state_dict']) for el in nn_weights]
    nn_weights = sorted(nn_weights, key=lambda x: x[0] if sort_ascending else -x[0])
    
    nn_scores = [el[0] for el in nn_weights]
    nn_weights = [el[1] for el in nn_weights]
    
    if verbose: print(f'SWA score: {nn_scores}')
    
    if take_best is not None:
        if verbose: print('Solo')
        avaraged_dict = OrderedDict()
        for k in nn_weights[take_best].keys():
            if delete_module:
                new_k = k[len('module.'):]
            else:
                new_k = k
                
            avaraged_dict[new_k] = nn_weights[take_best][k]
    else:
        if verbose: print('SWA')
        n_nns = len(nn_weights)
        if n_nns < 2:
            raise RuntimeError('Please provide more then 2 checkpoints')

        avaraged_dict = OrderedDict()
        for k in nn_weights[0].keys():
            if delete_module:
                new_k = k[len('module.'):]
            else:
                new_k = k

            avaraged_dict[new_k] = sum(nn_weights[i][k] for i in range(n_nns)) / float(n_nns)

    torch.save({'model_state_dict':avaraged_dict}, save_path)