import sys
sys.path.append('../')

import torch
import numpy as np

from catalyst.dl import Callback, CallbackOrder

from basic_utils.metric import map_per_set, mean_average_precision_at_k, mapk

class MAP5Callback(Callback):
    def __init__(self):
        super().__init__(CallbackOrder.Metric)
        
        self.running_preds = []
        self.running_targets = []
        
    def on_batch_end(self, state):
        y_hat = state.output['logits'].detach().cpu()
        y_hat = torch.sort(-y_hat, 1).indices[:,:5].numpy()
        y = state.input['targets'].detach().cpu().numpy()

        self.running_preds.append(y_hat)
        self.running_targets.append(y)
            
    def on_loader_end(self, state):        
        y_true = np.concatenate(self.running_targets)
        y_pred = np.concatenate(self.running_preds)
        
        state.loader_metrics['MAP5'] = map_per_set(
            list(y_true), 
            list(y_pred)
        )

        self.running_preds = []
        self.running_targets = []

class CinemaMAP5Callback(Callback):
    def __init__(self):
        super().__init__(CallbackOrder.Metric)
        
        self.running_preds = []
        self.running_targets = []
        
    def on_batch_end(self, state):
        y_hat = state.output['logits'].detach().cpu()
        y_hat = torch.sort(-y_hat, 1).indices[:,:5].numpy()
        y = state.input['targets'].detach().cpu().numpy()

        self.running_preds.append(y_hat)
        self.running_targets.append(y)
            
    def on_loader_end(self, state):        
        y_true = np.concatenate(self.running_targets)
        y_pred = np.concatenate(self.running_preds)
        
        state.loader_metrics['CMAP5'] = mapk(
            list(y_true), 
            list(y_pred)
        )

        self.running_preds = []
        self.running_targets = []