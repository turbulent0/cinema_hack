import numpy as np

def map_per_image(label, predictions):
    """Computes the precision score of one image.

    Parameters
    ----------
    label : string
            The true label of the image
    predictions : list
            A list of predicted elements (order does matter, 5 predictions allowed per image)

    Returns
    -------
    score : double
    """    
    predictions = list(predictions)
    try:
        return 1 / (predictions[:5].index(label) + 1)
    except ValueError:
        return 0.0

def map_per_set(labels, predictions):
    """Computes the average over multiple images.

    Parameters
    ----------
    labels : list
             A list of the true labels. (Only one true label per images allowed!)
    predictions : list of list
             A list of predicted elements (order does matter, 5 predictions allowed per image)

    Returns
    -------
    score : double
    """
    return np.mean([map_per_image(l, p) for l,p in zip(labels, predictions)])


def precision_at_k(predicted_labels, true_labels, k=5):
    """
    Arguments:
        predicted_labels(list): list with predicted items in descending relevance order
        true_labels(list): list with relevant labels
    
    """
    
    top_k_labels = predicted_labels[:k]
    
    relevant_labels_in_top_k = len(set(top_k_labels).intersection(set(true_labels[:k])))
    
    return relevant_labels_in_top_k / k


def average_precision_at_k(ranked_labels, true_labels, k=5):
    
    """
    Arguments:
        predicted_labels(list): list with predicted items in descending relevance order
        true_labels(list): list with relevant labels
    
    """
    
    precision_at_ks = [precision_at_k(ranked_labels, true_labels, k=i +1 )
                       for i, label in enumerate(ranked_labels) if label in true_labels]
    
    
    average_precision = sum(precision_at_ks) / k
    
    return average_precision


def mean_average_precision_at_k(true_labels, ranked_labels, k=5):
    """
    Arguments:
        predicted_labels(list): list of lists  with predicted items in descending relevance order for each user
        true_labels(list): list of lists with relevant labels for each user
    
    """
    
    average_precision_per_user = [average_precision_at_k(predicted_relevance, true_relevance, k=k) for 
                                  predicted_relevance, true_relevance in zip(ranked_labels, true_labels)]
    
    mean_average_precision = sum(average_precision_per_user) / len(ranked_labels)
    
    return mean_average_precision

def apk(actual, predicted, k=10):
    """
    Computes the average precision at k.
    This function computes the average prescision at k between two lists of
    items.
    Parameters
    ----------
    actual : list
             A list of elements that are to be predicted (order doesn't matter)
    predicted : list
                A list of predicted elements (order does matter)
    k : int, optional
        The maximum number of predicted elements
    Returns
    -------
    score : double
            The average precision at k over the input lists
    """
    if len(predicted)>k:
        predicted = predicted[:k]

    score = 0.0
    num_hits = 0.0

    for i,p in enumerate(predicted):
        if p in actual and p not in predicted[:i]:
            num_hits += 1.0
            score += num_hits / (i+1.0)

    if not actual:
        return 0.0

    return score / min(len(actual), k)

def mapk(actual, predicted, k=5):
    """
    Computes the mean average precision at k.
    This function computes the mean average prescision at k between two lists
    of lists of items.
    Parameters
    ----------
    actual : list
             A list of lists of elements that are to be predicted 
             (order doesn't matter in the lists)
    predicted : list
                A list of lists of predicted elements
                (order matters in the lists)
    k : int, optional
        The maximum number of predicted elements
    Returns
    -------
    score : double
            The mean average precision at k over the input lists
    """
    return np.mean([apk(list(a),list(p),k) for a,p in zip(actual, predicted)])