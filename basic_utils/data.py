import math

from tqdm import tqdm
from typing import Optional, Mapping
from copy import deepcopy

import pandas as pd
import numpy as np

def slice_sequances(
    df_p: pd.DataFrame,
    pad_value: int = -1,
    seq_len: int = 134,
    min_len: int = 4,
    step_size: int = 10,
    y_size: Optional[int] = 5,
    movie_mapping: Optional[Mapping[int,int]]= None,
    increaser: Optional[int] = None
):
    df = deepcopy(df_p)

    if movie_mapping is None:
        if pad_value == 0:
            increaser = 1
        else:
            increaser = 0
        movie_mapping = {f_id:(n_id + increaser) for n_id, f_id in enumerate(df['movie_id'].unique())}

    df['movie_id'] = df['movie_id'].map(movie_mapping)

    if y_size is not None and y_size >= min_len:
        raise RuntimeError('`seq_len` smaller or equal to `y_size`')

    grouped_by_users = df.groupby('user_id')['movie_id'].apply(list).reset_index()
    grouped_by_users['len'] = grouped_by_users['movie_id'].apply(len)
    
    sequences2use = grouped_by_users.loc[grouped_by_users['len'] > min_len, 'movie_id'].tolist()
    result_X = []
    if y_size is not None:
        result_y = []

    for init_seq in tqdm(sequences2use):
        len_init_seq = len(init_seq)
        if len_init_seq > seq_len:
            subseq_num = math.ceil((len_init_seq - seq_len) / step_size) + 1
            for i in range(subseq_num):
                if i == 0:
                    subseq = init_seq[:seq_len]
                else:
                    subseq = init_seq[i*step_size + seq_len:seq_len + (i+1)*step_size]

                if len(subseq) >= min_len:
                    
                    if len(subseq) < seq_len:
                        subseq = [pad_value]*(seq_len - len(subseq)) + subseq

                    result_X.append(np.array(subseq[:-y_size]))
                    if y_size is not None:
                        result_y.append(np.array(subseq[-y_size:]) - increaser)
        elif len_init_seq == seq_len:
            subseq = init_seq

            result_X.append(np.array(subseq[:-y_size]))
            if y_size is not None:
                result_y.append(np.array(subseq[-y_size:]) - increaser) 

        elif len_init_seq < seq_len:
            subseq = [pad_value]*(seq_len - len_init_seq) + init_seq

            result_X.append(np.array(subseq[:-y_size]))
            if y_size is not None:
                result_y.append(np.array(subseq[-y_size:]) - increaser)

            
    return result_X, result_y, movie_mapping

def anton_slicer(
    df_p: pd.DataFrame,
    pad_value: int = -1,
    maxlen: int = 18,
    min_len: int = 5,
    movie_mapping: Optional[Mapping[int,int]]= None,
    increaser: Optional[int] = None
):
    df = deepcopy(df_p)

    if movie_mapping is None:
        if pad_value == 0:
            increaser = 1
        else:
            increaser = 0
        movie_mapping = {f_id:(n_id + increaser) for n_id, f_id in enumerate(df['movie_id'].unique())}

    df['movie_id'] = df['movie_id'].map(movie_mapping)

    grouped_by_users = df.groupby('user_id')['movie_id'].apply(list).reset_index()
    grouped_by_users['len'] = grouped_by_users['movie_id'].apply(len)
    
    sequences2use = grouped_by_users.loc[grouped_by_users['len'] > min_len, 'movie_id'].tolist()

    X = []
    y = []

    def slice_sequence(seq, num_slices):
        for i in range(1, num_slices):
            temp_seq = seq[-(i+maxlen): -i]
            temp_seq = [pad_value]*(maxlen - len(temp_seq)) + temp_seq
            X.append(temp_seq)
            y.append([seq[-i] - increaser])

    for seq in sequences2use:
        if len(seq) <= 5:
            slice_sequence(seq, 2)
        elif len(seq) <= 6:
            slice_sequence(seq, 3)
        elif len(seq) <= 8:
            slice_sequence(seq, 4)
        elif len(seq) <= 12:
            slice_sequence(seq, 6)
        elif len(seq) <= 16:
            slice_sequence(seq, 8)
        elif len(seq) <= 20:
            slice_sequence(seq, 11)
        elif len(seq) <= 26:
            slice_sequence(seq, 16)
        else:
            slice_sequence(seq, 23)

    return X, y, movie_mapping